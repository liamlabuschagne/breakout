export default class Brick{
    constructor (game,position){
        this.game = game;
        this.image = document.getElementById("img_brick");
        this.positition = position;
        this.width = 80;
        this.height = 24;
    }
    
    update(deltaTime){
        
    }
    
    draw(ctx){  ctx.drawImage(this.image,this.positition.x,this.positition.y,this.width,this.height);
    }
    
}