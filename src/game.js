import Paddle from '/src/paddle.js';
import InputHandler from '/src/input.js';
import Ball from '/src/ball.js';
import Brick from '/src/brick.js';

import {buildLevel,level1} from '/src/levels.js';

export default class Game {
    constructor (gameWidth,gameHeight){
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
    }
    
    start(){
        this.paddle = new Paddle(this);
        this.ball = new Ball(this)
        
        
        let bricks = buildLevel(this,level1);

        new InputHandler(this.paddle);
        
        this.gameObjects = [this.ball,this.paddle,...bricks];
    }
    
    update(deltaTime){
        this.gameObjects.forEach((obj)=> obj.update(deltaTime));
    }
    
    draw(ctx){
        this.gameObjects.forEach((obj)=> obj.draw(ctx));
    }
}