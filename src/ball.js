import {detectCollision} from '/src/collisionDetection.js';

export default class Ball {
    constructor (game){
        this.gameWidth = game.gameWidth;
        this.gameHeight = game.gameHeight;
        this.game = game;
        this.image = document.getElementById("img_ball");
        this.speed = {
            x:64,
            y:64
        }
        this.positition = {
            x:10,
            y:10
        }
        this.size = 16;
    }
    
    draw(ctx){
        ctx.drawImage(this.image,this.positition.x,this.positition.y,this.size,this.size);
    }
    
    update(deltaTime){
        this.positition.x += this.speed.x / deltaTime;
        this.positition.y += this.speed.y / deltaTime;
        
        if(this.positition.x + this.size > this.gameWidth || this.positition.x < 0){
            this.speed.x = -this.speed.x;
        }
        
        if(this.positition.y + this.size > this.gameHeight || this.positition.y < 0){
            this.speed.y = -this.speed.y;
        }
        
//        let bottomOfBall = this.positition.y + this.size;
//        let topOfPaddle = this.game.paddle.position.y;
//        
//        let leftSideOfPaddle = this.game.paddle.position.x;
//        let rightSideOfPaddle = this.game.paddle.position.x + this.game.paddle.width;
//        if(bottomOfBall >= topOfPaddle
//          && this.positition.x >= leftSideOfPaddle
//          && this.positition.x + this.size <= rightSideOfPaddle){
//            this.speed.y = -this.speed.y;
//            this.positition.y = this.game.paddle.position.y-this.size;
//        }
        
        if(detectCollision(this,this.game.paddle)){
           this.speed.y = -this.speed.y;
           this.positition.y = this.game.paddle.position.y-this.size;
        }
        
    }
}