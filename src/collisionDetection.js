export function detectCollision(ball,gameObject){
    let bottomOfBall = ball.positition.y + ball.size;
    let topOfBall = ball.position.y;
    
    let topOfObject = gameObject.position.y;
    let bottomOfObject = gameObject.position.y + gameObject.height;
    let leftSideOfObject = gameObject.position.x;
    let rightSideOfObject = gameObject.position.x + gameObject.width;
    if(bottomOfBall >= topOfObject
        && topOfBall <= bottomOfObject
        && ball.positition.x >= leftSideOfObject
        && ball.positition.x + ball.size <= rightSideOfObject){
        return true;
    }else {
        return false;
    }
}